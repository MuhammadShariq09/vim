<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProject extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('projects', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('category_id');
            $table->foreign('category_id')->references('id')->on('categories');
            $table->string('name')->nullable();
            $table->string('lisence')->nullable();
            $table->string('phone')->nullable();
            $table->string('tax')->nullable();
            $table->integer('age')->length(5)->nullable();
            $table->string('zipcode')->nullable();
            $table->integer('no_of_employees')->length(10)->nullable();
            $table->string('fund_needed')->length(10)->nullable();
            $table->string('fund_plan')->length(5)->nullable();
            $table->text('reason')->nullable();
            $table->text('address')->nullable();
            $table->string('website')->length(50)->nullable();
            $table->unsignedBigInteger('band_id')->unsigned();
            $table->foreign('band_id')->references('id')->on('bands');
            $table->integer('turnover')->nullable();
            $table->boolean('term1')->nullable();
            $table->boolean('term2')->nullable();
            $table->string('title')->nullable();
            $table->string('category')->nullable();
            $table->string('property_type')->nullable();
            $table->string('area')->nullable();
            $table->integer('price_before_12_months')->nullable();
            $table->integer('price_after_1_year')->nullable();
            $table->tinyInteger('status')->default(0);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('projects');
    }
}
