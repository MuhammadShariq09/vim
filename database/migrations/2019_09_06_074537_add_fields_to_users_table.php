<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            //0 pending
            //1 approve
            $table->tinyInteger('status')->default(0)->nullable();
            $table->string('country')->nullable();
            $table->softDeletes();
        });
        Schema::table('investors', function (Blueprint $table) {
            //0 pending
            //1 approve
            $table->tinyInteger('status')->default(0)->nullable();
            $table->string('country')->nullable();
            $table->softDeletes();
        });
        Schema::table('lawyers', function (Blueprint $table) {
            //0 pending
            //1 approve
            $table->tinyInteger('status')->default(0)->nullable();
            $table->string('country')->nullable();
            $table->softDeletes();
        });                
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {});
        Schema::table('investors', function (Blueprint $table) {});
        Schema::table('lawyers', function (Blueprint $table) {});
    }
}
