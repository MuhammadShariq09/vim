<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
   return __('VIM');
});

Auth::routes();

Route::post('loginForApi', 'Auth\LoginController@loginForApi');
Route::get('logoutForApi', 'Auth\LoginController@logoutForApi');

Route::get('logout', 'Auth\LoginController@logoutForApi');
Route::get('/home', 'HomeController@index')->name('home');

Route::get('project/categories/{id?}', 'ProjectOwner\ProjectController@category');    
Route::resource('project', 'ProjectOwner\ProjectController');    
