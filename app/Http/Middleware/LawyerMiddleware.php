<?php

namespace App\Http\Middleware;

use Closure;

class LawyerMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if ($guard !== 'lawyer' && auth()->guard($guard)->check()){
            return redirect()->route('lawyer.login');
        }
        return $next($request);        
    }
}
