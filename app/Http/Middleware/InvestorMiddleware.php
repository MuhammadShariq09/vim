<?php

namespace App\Http\Middleware;

use Closure;

class InvestorMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if ($guard !== 'investor' && auth()->guard($guard)->check()){
            return redirect()->route('investor.login');
        }
        return $next($request);        
    }
}
