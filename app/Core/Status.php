<?php

namespace App\Core;

/**
 * Class Status.
 */
class Status
{
    const PENDING = 0;
    const ACCEPTED = 1;
    const DENIED = 2;
    const REJECTED = 2;
    const IN_VOTING = 3;
    const SUBMITTED = 4;
    const CLOSED = 5;
    const COMPLETED = 5;
    const BLOCKED = 6;
    const DISPUTE = 6;
    const DEFEATED = "DEFEATED";
    const WINNER = "WINNER";
}
