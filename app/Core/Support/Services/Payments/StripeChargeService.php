<?php


namespace App\Core\Support\Services\Payments;


use App\Models\CreditCard;
use App\User;
use Illuminate\Http\Request;
use Stripe\Stripe;

class StripeChargeService
{

    /**
     * @var Request
     */
    private $request;
    /**
     * @var CreditCard
     */
    private $user;
    /**
     * @var CreditCard
     */
    private $creditCard;

    /**
     * StripeChargeService constructor.
     * @param Request $request
     */
    public function __construct(Request $request)
    {

        Stripe::setApiKey(config('services.stripe.secret'));
        $this->request = $request;
        $this->user = auth()->user() ?? new User;
    }

    public function charge(){

    }

    public function setCardCard(CreditCard $card)
    {
        $this->creditCard = $card;
    }

    public function firstOrCreate(string $email)
    {
        return \Stripe\Customer::retrieve('cus_FZDHxufGfEKgov');
    }

}
