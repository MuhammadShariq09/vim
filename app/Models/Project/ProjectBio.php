<?php

namespace App\Models\Project;

use Illuminate\Database\Eloquent\Model;

class ProjectBio extends Model
{
    protected $fillable = ['project_id', 'title', 'who_we_are', 'why_seeking_finance', 'decl_1', 'decl_2' ];

    protected $table = 'project_bio';
}
