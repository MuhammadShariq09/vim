<?php

namespace App\Models\Project;

use Illuminate\Database\Eloquent\Model;

class ProjectPartner extends Model
{
    protected $fillable = ['project_id', 'owner_type', 'firstname', 'lastname', 'contact_no', 'email', 'address', 'tax'];
    
    protected $table = 'project_partners';    
}
